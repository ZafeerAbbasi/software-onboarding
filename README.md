# SFU Robot Soccer Onboarding Challenge

The goal of this challenge is to get you familiar with contributing on GitLab and to evaluate your programming skills.

## Starting steps

1. Learn how we work.
2. Learn what we expect.
3. Show us what you got!

## How we work
In the software team of SFU Robot Soccer we work together through GitLab and organize through live meetings, our chat on Discord as well as using the issue board on GitLab. All of the information you need should be available asynchronously and you should be able to contribute asynchronously too.

## What we expect
Our main codebase `software` has documentation on how to contribute to our project. These rules are open for change given a good motivating argument.

The main expectations in our team are:
- Adhering to our [coding style](https://google.github.io/styleguide/cppguide.html#Inputs_and_Outputs), this minimizes conflict and makes code easier to read for all
    - You can install [cpplint](https://github.com/cpplint/cpplint) to automatically enforce style on your machine
- Doing the best you can
- Being open to constructive feedback

## Show us what you got

We currently have one challenge to choose from – the Robot Bounded in Circle problem. The problem statement and necessary files are contained in the directory.

### The birth of a task - Issue and merge request creation on GitLab
Start by creating a new issue (synonymous to task/story) in this repository, click on the issues tab on the left and click the blue New Issue button.

Title your issue "Add solution for robot bounded in circle problem - `your_name`" and write a description describing what is being solved here (pertaining to the selected challenge).
It is important to be able to write clear descriptions for issues you create, so that other developers won't have to contact you in the future asking to clarify what the task and it's requirements are long after you've forgotten it.

Once you create the issue you will be redirected to the page of your issue.

Click the dropdown next to create merge request to ensure that "Create merge request and new branch" is selected, then click it. This will take you to a new page with your merge request.
A merge request contains all the activity related to your work on the specific issue.

### Local development

If you haven't done so, `git clone` the repository locally and then `git checkout` your branch.

For the challenge write your solutions in the solution.cc file

You can compile and test your solution by running `make`

Remove the test binary by running `make clean` or just type `rm ./test`.

### Submission

Publish your code by running `git push -u origin YOUR_BRANCH_NAME_HERE`, you can keep doing this before you have a complete solution so that your work is backed up.

Once ready, assign Matthew (@Zounon) as the reviewer for your code and mark it as ready.

Await feedback from the review, and then respond to it with any clarifications and changes to your code.

You will receive feedback about your coding style and how you solved the problem. We hope this helps you get familiar with the development workflow at SFURS.
